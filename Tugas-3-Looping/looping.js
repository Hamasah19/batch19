var angka = 2
console.log( 'LOOPING PERTAMA')
while (angka <= 20){
  console.log( angka + ' - I love coding'  )
  angka+=2
}

//
var nilai = 20
console.log( 'LOOPING KEDUA')
while (nilai > 0){
  console.log( nilai + ' - I will become a mobile developer'  )
  nilai-=2
}


console.log("Looping menggunakan for")

for (i=1;i<=20;i++){
    if (i%2==0){
        console.log(i+" Santai")
    }else if (i%3==0){
        console.log(i+" I Love Coding")
      }
      else{
        console.log(i+" Berkualitas")
      }       
}