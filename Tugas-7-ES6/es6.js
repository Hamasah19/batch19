//Soal No. 1 Mengubah fungsi menjadi fungsi arrow

const golden = goldenFunction = () => {
    console.log("this is golden!!")
  }
   
  golden()


//Soal No. 2 - Sederhanakan menjadi Object literal

// const fullName = 'John Doe'
 
// const john = {
//   fullName: fullName
// }


// const newFunction = function literal(firstName, lastName){
//   //return {
//     //firstName: firstName,
//     //lastName: lastName,
//     const fullName =  newFunction
//       //console.log(firstName + " " + lastName)
      //return 
   // }
  
// }
 
//Driver Code 
// newFunction("William", "Imoh").fullName() 

//Soal No. 3 - Destructing
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName,lastName,destination,occupation,spell} = newObject;

console.log(firstName,lastName,destination,occupation,spell)


//Soal No. 4 - Array Spreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combine =[...west, ...east]
console.log(combine)


// Soal No. 5 - Template Literal

const planet = 'earth'
const view = 'glass'

const before = `Lorem ${view} dolor sit amet consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before)
