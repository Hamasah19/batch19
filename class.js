var Car = class {
    constructor(brand, factory) {
        this.brand = brand
        this.factory = factory
    }
}

myCar = new Car('Ferari', 'Italia')
console.log(myCar.brand, myCar.factory)