//Soal1
var word = 'JavaScript '; 
var second = 'is '; 
var third = 'awesome '; 
var fourth = 'and '; 
var fifth = 'I '; 
var sixth = 'love '; 
var seventh = 'it!';

console.log(word.concat(second,third,fourth,fifth,sixth,seventh));


//Soal 2
var sentence = "I am going to be React Native Developer"; 

var FirstWord = sentence[0] ; 
var SecondWord = sentence[2] + sentence[3];
var ThirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9]
var FourthWord = sentence[11] + sentence[12];
var FifthWord = sentence[14] + sentence[15];
var SixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21]
var SeventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28]
var EightWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38]

console.log('First Word: ' + FirstWord); 
console.log('Second Word: ' + SecondWord); 
console.log('Third Word: ' + ThirdWord); 
console.log('Fourth Word: ' + FourthWord); 
console.log('Fifth Word: ' + FifthWord); 
console.log('Sixth Word: ' + SixthWord);
console.log('Seventh Word: ' + SeventhWord);
console.log('Eighth Word: ' + EightWord);


//Soal 3
var sentence2 = 'wow JavaScript is so cool'; 

var FirstWord2 = sentence2.substring(0, 3); 
var SecondWord2 = sentence2.substring(4, 14); 
var ThirdWord2 = sentence2.substring(15, 17); 
var FourthWord2 = sentence2.substring(18, 20); 
var FifthWord2 = sentence2.substring(21, 25); 

console.log('First Word: ' + FirstWord2);
console.log('Second Word: ' + SecondWord2);
console.log('Third Word: ' + ThirdWord2);
console.log('Fourth Word: ' + FourthWord2);
console.log('Fifth Word: ' + FifthWord2);

//Soal4
var sentence3 = 'wow JavaScript is so cool'; 

var FirstWord3 = sentence3.substring(0, 3); 
var SecondWord3 = sentence3.substring(4, 14); 
var ThirdWord3 = sentence3.substring(15, 17); 
var FourthWord3 = sentence3.substring(18, 20); 
var FifthWord3 = sentence3.substring(21, 25); 

var firstWordLength = FirstWord3.length
var secondWordLength = SecondWord3.length
var thirdWordLength = ThirdWord3.length
var fourthWordLength = FourthWord3.length
var fifthWordLength = FifthWord3.length

console.log('First Word: ' + FirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + SecondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + ThirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + FourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + FifthWord3 + ', with length: ' + fifthWordLength); 