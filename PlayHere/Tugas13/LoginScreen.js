import React from 'react';
import {Platform, 
    StyleSheet, 
    Text, 
    View, 
    Image, 
    TextInput, 
    TouchableOpacity, 
    Button, 
    KeyboardAvoidingView, 
    ScrollView} from  'react-native'
import { color } from 'react-native-reanimated';


const LoginScreen = () => {
    return (
        <KeyboardAvoidingView 
        behavior = {Platform.OS =="ios" ? "padding" : 'height'}
        style={styles.container}
        >
            <ScrollView>

            
            <View style = {styles.containerView}>
                <Image source ={require('./images/logo.png')} />
                <Text>Login</Text>
                <View style = {styles.formInput}>
                    <Text style={styles.formtext}>Username</Text>
                    <TextInput style={styles.formtext} />
                </View>
                <View style = {styles.formInput}>
                    <Text style={styles.formtext}>Password</Text>
                    <TextInput style={styles.formtext} secureTextEntry={true}/>
                </View>
                <View style={styles.kotaklogin}>
                    <TouchableOpacity style={styles.btlogin}>
                        <Text style ={styles.textbt}>Masuk</Text>
                    </TouchableOpacity>
                    <Text style={styles.autotext}>atau</Text>
                    <TouchableOpacity style={styles.btreg}>
                        <Text style={styles.textbt}>Daftar</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </ScrollView>
        </KeyboardAvoidingView>
    )
}

export default LoginScreen;

const styles = StyleSheet.create({
    containerView : {
        flex : 1,
        color : '#e3ffc6'
    },
    logintext  : {
        fontSize : 24,
        marginTop : 63,
        textAlign : 'center',
        color : '#003366',
        marginVertical : 20
    },
    formtext : {
        color : '#003366',
    },
    autotext : {
        fontSize : 20,
        color : '#3EC6FF'
    },
    formInput :{
        marginHorizontal : 30,
        marginVertical : 5,
        alignContent :'center',
        width : 294
    },
    input :{
        height : 40,
        borderColor : 'E003366',
        padding : 10,
        borderEndWidth : 1
    }
})
